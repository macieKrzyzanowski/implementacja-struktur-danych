#include <iostream>
#include <cstdlib>
#include <ctime>
#include "kolejka_prior.h"

using namespace std;

template <typename T>
void PriorityQueueSort(Sekwencja <T> & S)
{
    KolejkaPrior<int> P;
    while(!S.isEmpty())
    {
        P.insert(S.front(),S.front());
        S.removeFront();

    }
    while(!P.isEmpty())
    {
        S.addFront(P.min());
        P.removeMin();
    }
}

int main()
{
    Sekwencja <int> LosoweElem;
    srand(time(NULL));

    for(int i=0; i<15; i++) //wstawianie do listy 15 liczb losowych
        LosoweElem.addFront((rand()%20)+1);

    cout << "Zawartosc sekwencji przed sortowaniem: "<< endl;
    LosoweElem.show();
    cout<<endl;

    PriorityQueueSort(LosoweElem);

    cout << "Zawartosc sekwencji po sortowaniem: "<< endl;
    LosoweElem.show();

    return 0;
}
