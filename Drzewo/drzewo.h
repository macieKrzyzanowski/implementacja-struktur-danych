#include <iostream>
#include <exception>
#include "deque.h"

using namespace std;

class DontExistException;
class HasDescendantsException;

template <typename Typ>
class Drzewo;

template <typename Typ>
class ElementDrzewa
{
private:
    Typ wartosc;
    ElementDrzewa *ojciec;
    Deque<ElementDrzewa*> potomkowie;
    friend class Drzewo<Typ>;

public:
    /*********************** Metody dostepu **************************/
    Typ getWartosc() const {return wartosc;}
    ElementDrzewa *getOjciec() const {return ojciec;}
    Deque<ElementDrzewa*> getPotomkowie() const {return potomkowie;}
    void setWartosc(Typ nowyW) {wartosc = nowyW;}
    void setOjciec(ElementDrzewa *nowyO) {ojciec = nowyO;}
    void setPotomkowie(Deque<ElementDrzewa*> nowyP) {potomkowie = nowyP;}


    ElementDrzewa* visit(int index) //metoda zwraca adres potomka o podanym indexie
    {
        if(index >= potomkowie.size()) throw DontExistException();
        else return potomkowie[index];
    }

    ElementDrzewa()
    {
        wartosc = 0;
        ojciec = NULL;
    }
};


template <typename Typ>
class Drzewo: public ElementDrzewa<Typ>
{
private:
    ElementDrzewa<Typ> *korzen;
    int iloscElementow;

protected:
    /*********************** Metody dostepu **************************/
    ElementDrzewa<Typ> *getKorzen() const {return korzen;}
    int getIloscElementow() const {return iloscElementow;}
    void setKorzen(ElementDrzewa<Typ> *nowyK) {korzen = nowyK;}
    void setIloscElementow(int nowyI) {iloscElementow = nowyI;}

public:

    Drzewo()
    {
        korzen = NULL;
        iloscElementow = 0;
    }

    void addRoot(Typ); //dadaje nowy korzen
    void add(Typ,ElementDrzewa<Typ>*); //metoda dodaje element do drzewa o (o wartosc,do ojca)
    void remove(ElementDrzewa<Typ>*); //metoda usuwa wskazany element
    ElementDrzewa<Typ>* root() {return korzen;} //metoda zwraca wskaznik na korzen
    bool isExternal(ElementDrzewa<Typ> *wsk) const {return !wsk->potomkowie.size();} //funkca sprawdza czy podany element drzewa jest lisciem
    bool isEmpty() {return !iloscElementow;} //metoda sprawdza czy drzewo jest puste
    int size() {return iloscElementow;} //metoda zwraca ilosc elementow
    int sizeChildren(ElementDrzewa<Typ> *wsk) const {return wsk->potomkowie.size();} //metoda zwraca ilosc potomow danego elementu
    void printInElement(ElementDrzewa<Typ>*); //metoda podaje wartosc ojca i potomkow
};

template <typename Typ>
void Drzewo<Typ>::addRoot(Typ wart)
{
	ElementDrzewa<Typ>* nowy = new ElementDrzewa<Typ>;
	if(korzen == NULL)
    {
        nowy->setOjciec(NULL);
        nowy->setWartosc(wart);
        korzen = nowy;
        iloscElementow++;
    }
    else cerr << "Korzen juz istnieje." << endl;
}

template <typename Typ>
void Drzewo<Typ>::add(Typ wart, ElementDrzewa<Typ> *wsk)
{
	ElementDrzewa<Typ> *nowy = new ElementDrzewa<Typ>;
	if (korzen == NULL) addRoot(wart);
	else
    {
        nowy->setWartosc(wart);
		nowy->setOjciec(wsk);
		wsk->potomkowie.addBack(nowy);
		iloscElementow++;
	}
}

template <typename Typ>
void Drzewo<Typ>::remove(ElementDrzewa<Typ>* wsk)
{
	ElementDrzewa<Typ> *przodek = wsk->getOjciec();
	if (!isExternal(wsk)) throw HasDescendantsException();
	else
    {
        int i = 0;
		while (przodek->visit(i) != wsk) i++;
		przodek->potomkowie.erase(i);
	}
	iloscElementow--;
}

template<typename Typ>
void Drzewo<Typ>::printInElement(ElementDrzewa<Typ>* wsk)
{
    ElementDrzewa<Typ> *nowy = wsk;
    cout << "Ojciec: " << nowy->getWartosc() << endl;
    cout << "-------------------------" ;
    int iloscPotomkow = sizeChildren(nowy);

    for(int i=1; i<=iloscPotomkow; i++)
    {
        cout << endl << i << ".potomek: " << nowy->visit(i-1)->getWartosc();
    }
    cout << endl << "-------------------------" << endl;
}

//Metoda zwraca wysykosc grzewa tree o wsk. na korzen
template <typename Typ>
int height(const Drzewo<Typ> &tree, ElementDrzewa<Typ>* wsk)
{
	if (tree.isExternal(wsk)) return 0;
	else
    {
		int h = 0;
		for (int i=0; i<tree.sizeChildren(wsk); i++)
			h = max(h, height(tree, wsk->visit(i)));
		return 1+h;
	}
}

//procedura przejscia PreOrder
template <typename Typ>
void printPreOrder(const Drzewo<Typ> &tree, ElementDrzewa<Typ>* wsk)
{
    cout<< wsk->getWartosc();
    for (int i=0; i<tree.sizeChildren(wsk); i++)
    {
        cout << " ";
        printPreOrder(tree,wsk->visit(i));
    }
}

//procedura przejscia InOrder
template <typename Typ>
void printPostOrder(const Drzewo<Typ> &tree, ElementDrzewa<Typ>* wsk)
{
    for (int i=0; i < tree.sizeChildren(wsk); i++)
    {
        printPostOrder(tree,wsk->visit(i));
        cout << " ";
    }
    cout << wsk->getWartosc();
}


class DontExistException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Podany element drzewa nie istnieje.";
	}
};

class HasDescendantsException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Element ma potomkow.";
	}
};

