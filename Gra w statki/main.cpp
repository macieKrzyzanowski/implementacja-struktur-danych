/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#include <iostream>

#include "plansza.h"
#include "gracz.h"
#include "gracz_ai.h"

using namespace std;

int main()
{
    bool wygrana1, wygrana2;

    Plansza p1;
    Plansza p2;

    Gracz g1;
    Gracz_AI g2;

    //petla wykonuje sie dopoki ktorys z graczy wygra
    do
    {
        p1.rysuj_plansze_przeciwnika();
        cout << endl;
        p1.informacje();
        cout << endl << endl;
        p2.rysuj_plansze_gracza();
        cout << endl;
        p2.informacje();
        cout << endl << endl;

        cout << "Legenda:\n";
        cout << "[x] - trafiony statek\n";
        cout << "[o] - polozenie statku\n";
        cout << "[.] - strzal\n";

        wygrana1 = g1.strzelaj(p1);
        Sleep(1000);
        cout << endl;
        wygrana2 = g2.strzelaj(p2);
        Sleep(500);
        system("cls");

    }
    while (wygrana1 == false && wygrana2 == false);

    if(wygrana1 == true)
    {
        cout << "WYGRALES !!!\n";
        Sleep(5000);
    }

    if(wygrana2 == true)
    {
        cout << "PRZEGRALES\n";
        Sleep(5000);
    }

    return 0;
}
