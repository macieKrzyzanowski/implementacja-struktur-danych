/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#include <iostream>

using namespace std;

class Statek
{
private:
    const int dlugosc;
    bool *segment; //tablica przechowuje informacje o statusie segmentu statku (false - zniszczony; true - niezniszczony)
    int ID; //zmienna przechowuje identyfikator statku

public:
//metody dostepu:
    int getDlugosc() {return dlugosc;}
    bool getSegment(int wsk) {return segment[wsk];}
    int getID() {return ID;}
    void setID(int aID) {ID = aID;}

//metody publiczne:
    //metoda zmienia statku segmentu trafionego statku i sprawdza czy statek zostal zniszczony
    //IN: wsk-numer trafionego segmentu
    //OUT: false - statek zostal zniszczony; true - statek nie zostal zniszczony
    bool trafienie(int wsk)
    {
        segment[wsk] = true;
        for(int i=0; i<getDlugosc(); i++)
            if(getSegment(i) == false) return false;
        return true;

    }
    Statek(int dl) : dlugosc(dl)
    {
        //alokacja statku o podanej dlugosci
        segment = new bool[dlugosc];
        for(int i=0; i<dlugosc; i++)
            segment[i] = false;
    }

};

//dziedziczone klasy rodzajow statkow, rozniace sie dlugoscia

class Cztero_Masztowiec : public Statek
{
public:
    //alokacja 4 segmentowego statku
    Cztero_Masztowiec() : Statek(4) {}
};

class Trzy_Masztowiec : public Statek
{
public:
    Trzy_Masztowiec() : Statek(3) {}
};

class Dwu_Masztowiec : public Statek
{
public:
    Dwu_Masztowiec() : Statek(2) {}
};

class Jedno_Masztowiec : public Statek
{
public:
    Jedno_Masztowiec() : Statek(1) {}
};
