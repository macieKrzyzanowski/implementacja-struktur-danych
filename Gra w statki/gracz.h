/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#include <iostream>
#include <windows.h>
#include "plansza.h"

using namespace std;

class Gracz
{
public:
//metody publiczne:
    bool strzelaj(Plansza &p1); //funkja wykonuje strzal do planszy i informuje czy AI wygral
                                //IN: plansza przeciwnika
                                //OUT: false - nie wygral; true - wygral
};

bool Gracz::strzelaj(Plansza &p1)
{
    int x, y;
    bool czy_zatopiono;

    //petla wykonuje sie dopoki strzaly sa celne
    do{
        cout << "\nPodaj wsp. strzalu.\n";
        cout << "X: ";
        while(!(cin >> x)) //dop�ki strumie� jest w stanie b��du -> dop�ki podawane s� b��dne dane
        {
            cin.clear(); //kasowanie flagi b��du strumienia
            cin.sync(); //kasowanie zb�dnych znak�w z bufora
        }
        cout << "Y: ";
        while(!(cin >> y))
        {
            cin.clear();
            cin.sync();
        }

        cout << "[GRACZ]: ";
    }
    while(p1.strzal(y,x,czy_zatopiono));
    return p1.czy_wygrana(); //zwracanie inrofmacji czy gracz wygral
}
