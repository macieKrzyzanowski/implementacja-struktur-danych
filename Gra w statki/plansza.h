/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#ifndef PLANSZA_H
#define PLANSZA_H

#include <iostream>
#include <windows.h>
#include <conio.h>
#include <time.h>
#include <math.h>
#include "statki.h"

using namespace std;

class Pole
{
    bool flaga; //zmienna przechowuje informacje czy na danym polu znajduje sie czesc statku (false - brak statku; true - jest statek)
    bool strzal; //zmienna przechowuje informaje czy w dane pole juz strzelano (true - strzelano; false -nie strzelano)

    //zmienne przechowuja informacje o statku, ktory znajduje sie w danym polu:
    int rodzaj_statku; //informuje o rodzaju statku (4 - czteromasz. 3 - trzymasz. 2-dwumasz. 1-jednomasz.)
    int segment; //informuje o segmencie statku, ktory pole przechowuje
    int ID_statku; //identyfikator statku

public:

//metody dostepu:
    bool getFlaga() {return flaga;}
    bool getStrzal() {return strzal;}
    int getRodzaj_statku() {return rodzaj_statku;}
    int getSegment() {return segment;}
    int getID_statku() {return ID_statku;}
    void setFlaga(bool aflaga) {flaga = aflaga;}
    void setStrzal(bool astrzal) {strzal = astrzal;}
    void setRodzaj_statku(int arodzaj_statku) {rodzaj_statku = arodzaj_statku;}
    void setSegment(int asegment) {segment = asegment;}
    void setID_statku(int aID_statku) {ID_statku = aID_statku;}

    //inicjalizacja wartosci poczatkowych:
    Pole() : flaga(false), strzal(false),
             rodzaj_statku(0), segment(0), ID_statku(0) {}
};

class Plansza
{
private:
    Pole **tablica_gry; //tablica dwuwymiarowa przechowujaca informacje o kazdym polu planszy
    const int rozmiar; //rozmiar planszy

    //rodzaje statkow i ich ilosc
    //(poszczegolne rodzaje statkow przechowywane sa w tablicach)
    Cztero_Masztowiec *czteromasztowce;
    int ilosc_czteromasztowcow;

    Trzy_Masztowiec *trzymasztowce;
    int ilosc_trzymasztowcow;

    Dwu_Masztowiec *dwumasztowce;
    int ilosc_dwumasztowcow;

    Jedno_Masztowiec *jednomasztowce;
    int ilosc_jednomasztowcow;

    int ilosc_segmentow; //zmienna przechowuje laczna ilosc segmentow statkow na planszy

//metody pomocnicze
    bool sprawdz_poziom(int wspX, int wspY, int dlugosc_statku);
    //metoda sprawdza czy statek o podanej dlugosci mozna wstawic poziomo do planszy
    //w miejsce o podanych wsp.
    //IN: wspX,wspY - wsp. punktu; dlugosc_statku - dlugosc wstawianego statku
    //OUT: false - nie mozna wstawic w podane miejsce statku; true - mozna wstawic statek
    bool sprawdz_pion(int wspX, int wspY, int dlugosc_statku);
    //metoda analogiczna do powyzszej (sprawdza pion)
    void ustaw_statki();
    //metoda ustawia statki w posowym polozeniu
    void ustaw_statki_pomoc(int dlugosc_statku, int ID_statku);
    //metoda pomocnicza ustawiajaca statek o podanej dlugosci i identyfikatorze

public:
//metody publiczne:
    int rozmiar_planszy(); //metoda zwraca rozmiar planszy
    void rysuj_plansze_przeciwnika(); //metoda rysuje plansze przeciwnika (niewidoczne polozenie statkow)
    void rysuj_plansze_gracza(); //metoda rysuje plansze gracza (widoczne polozenie statkow)
    bool strzal(int x, int y, bool &czy_zatopiony);
    //metoda wykonuje strzal w pole o podanych wsp. oraz zwraca informacje czy statek zostal trafiony oraz czy zostal zatopiony
    //IN: x,y - wsp. strzalu
    //    czy_zatopiony - zmienna informuje czy statek zostal zatopiony
    //OUT: true - strzal trafiony
    //     false - strzal chybiony
    bool czy_wygrana(); //metoda sprawdza czy na planszy sa statki (OUT: false - brak statkow; true - sa statki)
    void informacje(); //metoda wyswietla informacje o podstawowych parametrach planszy


    Plansza(int rozm = 10) : rozmiar(rozm)
    {
        //tworzenie tablicy gry
        tablica_gry = new Pole*[rozmiar];
        for(int i=0; i<rozmiar; i++)
            tablica_gry[i] = new Pole[rozmiar];

        ilosc_czteromasztowcow = 1;
        ilosc_trzymasztowcow = 2;
        ilosc_dwumasztowcow = 3;
        ilosc_jednomasztowcow = 1;

        //alokacja tablic przechowujacych okreslone rodzaje statkow
        czteromasztowce = new Cztero_Masztowiec[ilosc_czteromasztowcow];
        trzymasztowce = new Trzy_Masztowiec[ilosc_trzymasztowcow];
        dwumasztowce = new Dwu_Masztowiec[ilosc_dwumasztowcow];
        jednomasztowce = new Jedno_Masztowiec[ilosc_jednomasztowcow];

        ilosc_segmentow = 0;
        ustaw_statki();
    }

    ~Plansza()
    {
        //dealokacja pamieci
        for ( int i=0; i<rozmiar; ++i)
            delete [] tablica_gry[i];
        delete [] tablica_gry;

        delete [] czteromasztowce;
        delete [] trzymasztowce;
        delete [] dwumasztowce;
        delete [] jednomasztowce;
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////

void Plansza::rysuj_plansze_przeciwnika()
{
    cout << "PLANSZA PRZECIWNIKA:\n";
    for(int i=0; i<rozmiar; i++)
    {
        cout << "[" << i << "] ";
        for(int j=0; j<rozmiar; j++)
        {
            if(tablica_gry[i][j].getFlaga() == true && tablica_gry[i][j].getStrzal() == true)
                cout << "[x] ";
            if(tablica_gry[i][j].getFlaga() == true && tablica_gry[i][j].getStrzal() == false)
                cout << "[ ] ";
            if(tablica_gry[i][j].getFlaga() == false && tablica_gry[i][j].getStrzal() == true)
                cout << "[.] ";
            if(tablica_gry[i][j].getFlaga() == false && tablica_gry[i][j].getStrzal() == false)
                cout << "[ ] ";
        }
        cout << endl;
    }
    cout << "    ";
    for(int i=0; i<rozmiar; i++)
        cout << "[" << i << "] ";
}

void Plansza::rysuj_plansze_gracza()
{
    cout << "TWOJA PLANSZA:\n";
    for(int i=0; i<rozmiar; i++)
    {
        cout << "[" << i << "] ";
        for(int j=0; j<rozmiar; j++)
        {
            if(tablica_gry[i][j].getFlaga() == true && tablica_gry[i][j].getStrzal() == true)
                cout << "[x] ";
            if(tablica_gry[i][j].getFlaga() == true && tablica_gry[i][j].getStrzal() == false)
                cout << "[o] ";
            if(tablica_gry[i][j].getFlaga() == false && tablica_gry[i][j].getStrzal() == true)
                cout << "[.] ";
            if(tablica_gry[i][j].getFlaga() == false && tablica_gry[i][j].getStrzal() == false)
                cout << "[ ] ";
        }
        cout << endl;
    }
    cout << "    ";
    for(int i=0; i<rozmiar; i++)
        cout << "[" << i << "] ";
}

////////////////////////////////////////////////////////////////////////////////////////////////////

void Plansza::ustaw_statki()
{
    int ID = 1;
    cout << "Budowa czteromasztowcow...\n";
    //petla ustawia okreslona w konstruktorze ilosc statkow:
    for(int i=0; i<ilosc_czteromasztowcow; i++)
    {
        czteromasztowce[i].setID(ID); //ustawienie id statku
        ustaw_statki_pomoc(czteromasztowce[i].getDlugosc(),czteromasztowce[i].getID()); //ustawienie statku
        Sleep(150); //uspienie watku (koniecznosc uzycia tej funkcji jest spowodowana tym, we funkcja ustawiajaca statki
                    //korzysta z funkcji srand() losowania pseldolosowego, ktora z kolei bazuje na pomiarze czasu)
                    //konieczne, aby statki nie byly ustawione zbyt blisko siebie
        ID++;
    }
    cout << "Budowa trzymasztowcow...\n";
    //analogicznie jak powyzej
    for(int i=0; i<ilosc_trzymasztowcow; i++)
    {
        trzymasztowce[i].setID(ID);
        ustaw_statki_pomoc(trzymasztowce[i].getDlugosc(),trzymasztowce[i].getID());
        Sleep(150);
        ID++;
    }
    cout << "Budowa dwumasztowcow...\n";
    //analogicznie jak powyzej
    for(int i=0; i<ilosc_dwumasztowcow; i++)
    {
        dwumasztowce[i].setID(ID);
        ustaw_statki_pomoc(dwumasztowce[i].getDlugosc(),dwumasztowce[i].getID());
        Sleep(150);
        ID++;
    }
    cout << "Budowa jednomasztowcow...\n";
    //analogicznie jak powyzej
    for(int i=0; i<ilosc_jednomasztowcow; i++)
    {
        jednomasztowce[i].setID(ID);
        ustaw_statki_pomoc(jednomasztowce[i].getDlugosc(),jednomasztowce[i].getID());
        Sleep(150);
        ID++;
    }

    system("cls");
}


void Plansza::ustaw_statki_pomoc(int dlugosc_statku, int ID_statku)
{
    int wspX, wspY, kierunek; //wsp przechowuje informacje, o miejscu w ktorym bedzie polozony poczatek statku (wspX - wsp. X; wspY - wsp. Y)
                              //kierunek wskazuje w jakiej pozycj ma sie znajdowac statek 0-pozomej; 1-pionowej.
    srand(time(NULL));

    //losowanie kierunku ustawiania statku
    kierunek = rand()%2;
    //losowanie wsp. poczatku statku
    //wartosc bezwzledna zapewnie nie przekroczenie rozmiaru tablicy gry
    wspX = abs((rand()%(rozmiar+1-dlugosc_statku)));
    wspY = abs((rand()%(rozmiar+1-dlugosc_statku)));

    /*warunek sprawdza jaka pozycje wylosowano*/
    //jezeli wylosowano kierunek poziomy
    if(kierunek == 0)
    {
        //petla sprawdza czy istnieje mozliwosc ustawienia statku w danych wsp.
        //jezeli nie ma takiej mozliwosci (statek koliduje z wczesniej ustawionym statkiem) losuje inne wartosci
        while(sprawdz_poziom(wspX,wspY,dlugosc_statku))
        {
            wspX = abs(rand()%(rozmiar+1-dlugosc_statku));
            wspY = abs(rand()%(rozmiar+1-dlugosc_statku));
        }
        //petla przechodzi przez wybrane pola tablicy gry, aby ustawic statek
        for(int i=0; i<dlugosc_statku; i++)
        {
            tablica_gry[wspX+i][wspY].setFlaga(true); //ustawia flage obecnosci statku w polu
            tablica_gry[wspX+i][wspY].setSegment(i); //ustawia numer segmentu statku
            tablica_gry[wspX+i][wspY].setRodzaj_statku(dlugosc_statku); //ustawia informacje o rodzaju statku
            tablica_gry[wspX+i][wspY].setID_statku(ID_statku); //ustawia identyfikator
            ilosc_segmentow++; //inkrementacja ilosci segmentow na planszy
        }
    }
    //jezeli wylosowano kierunek pionowy (analogicznie jak powyzej)
    else
    {
        while(sprawdz_pion(wspX,wspY,dlugosc_statku))
        {
            wspY = abs(rand()%(rozmiar+1-dlugosc_statku));
            wspY = abs(rand()%(rozmiar+1 - dlugosc_statku));
        }
        for(int i=0; i<dlugosc_statku; i++)
        {
            tablica_gry[wspX][wspY+i].setFlaga(true);
            tablica_gry[wspX][wspY+i].setSegment(i);
            tablica_gry[wspX][wspY+i].setRodzaj_statku(dlugosc_statku);
            tablica_gry[wspX][wspY+i].setID_statku(ID_statku);
            ilosc_segmentow++;
        }
    }
}

bool Plansza::sprawdz_poziom(int wspX, int wspY, int dlugosc_statku)
{
    //petla przechodzi przez pola, na ktorych chcemy ustawic statek
    for(int i=0; i<dlugosc_statku; i++)
    {
        if(tablica_gry[wspX+i][wspY].getFlaga() == true)
            return true; //jezeli przetnie zajete pole, zwraca true
    }
    return false; //jezeli wszystkie pola sa zajete
}

//funkca dziala podobnie do powyzszej
bool Plansza::sprawdz_pion(int wspX, int wspY, int dlugosc_statku)
{
    for(int i=0; i<dlugosc_statku; i++)
    {
        if(tablica_gry[wspX][wspY+i].getFlaga() == true)
            return true;
    }
    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

bool Plansza::strzal(int x, int y, bool &czy_zatopiono)
{
    //warunek sprawdza, czy nie przekroczono rozmiaru tablicy gry
    if(x >= 0 && x < rozmiar && y >= 0 && y < rozmiar)
    {
        //warunek sprawdza czy wykonywano juz strzal w pole o podanych wsp.
        if(tablica_gry[x][y].getStrzal() == true)
        {
            cout << "Strzelales juz w to pole!\n";
            czy_zatopiono = false;
            return true;
        }
        //jezeli nie wykonano jeszcze strzalu w podane pole
        else
        {
            //warunek sprawdza czy na podanym polu znajduje sie statek
            if(tablica_gry[x][y].getFlaga() == true)
            {
                tablica_gry[x][y].setStrzal(true); //ustawienie informacji o wykonaniu strzalu w pole

                int rodzaj_statku = tablica_gry[x][y].getRodzaj_statku();
                int ID_statku = tablica_gry[x][y].getID_statku();
                int segment = tablica_gry[x][y].getSegment();

                //warunek wielokrotnego wyboru, sprawdzajacy rodzaj trafionego statku
                switch(rodzaj_statku)
                {
                    //trafiono jednomasztowiec:
                    case 1:
                        cout << "TRAFIONY ZATOPIONY! (jednomasztowiec)\n";
                        czy_zatopiono = true;
                        ilosc_segmentow--;
                        return true;
                    break;
                    //trafiono dwumasztowiec:
                    case 2:
                        //petla przeszukuje tablice dwumasztowcow w celu znalezienia trafionego statku
                        for(int i=0; i<ilosc_dwumasztowcow; i++)
                        {
                            //jezeli zanjdzie
                            if (dwumasztowce[i].getID() == ID_statku)
                            {
                                //warunek sprawdza czy statek nie zostal zatopiony
                                //jezeli nie zostal zatopiony
                                if(!dwumasztowce[i].trafienie(segment))
                                {
                                    czy_zatopiono = false;
                                    cout << "TRAFIONY!\n";
                                }
                                //jezeli zostal zatopiony
                                else
                                {
                                    czy_zatopiono = true;
                                    cout << "TRAFIONY ZATOPIONY!\n";
                                }
                                ilosc_segmentow--;
                                return true;

                            }
                        }
                    break;
                    //trafiono trzymasztowiec:
                    case 3:
                        for(int i=0; i<ilosc_trzymasztowcow; i++)
                        {
                            if (trzymasztowce[i].getID() == ID_statku)
                            {
                                if(!trzymasztowce[i].trafienie(segment))
                                {
                                    czy_zatopiono = false;
                                    cout << "TRAFIONY!\n";
                                }
                                else
                                {
                                    czy_zatopiono = true;
                                    cout << "TRAFIONY ZATOPIONY!\n";
                                }
                                ilosc_segmentow--;
                                return true;
                            }
                        }
                    break;
                    //trafiono czteromasztowiec:
                    case 4:
                        for(int i=0; i<ilosc_czteromasztowcow; i++)
                        {
                            if (czteromasztowce[i].getID() == ID_statku)
                            {
                                if(!czteromasztowce[i].trafienie(segment))
                                {
                                    czy_zatopiono = false;
                                    cout << "TRAFIONY!\n";
                                }
                                else
                                {
                                    czy_zatopiono = true;
                                    cout << "TRAFIONY ZATOPIONY!\n";
                                }
                                ilosc_segmentow--;
                                return true;
                            }
                        }
                    break;
                }
            }
            //jezeli nie trafiono statku
            else
            {
                cout << "PUDLO!\n";
                tablica_gry[x][y].setStrzal(true);
                czy_zatopiono = false;
                return false;
            }
        }
    }
    //jezeli przekroczono rozmiar tablicy gry
    else
    {
        cout << "Wybrales pkt. poza plansza!";
        czy_zatopiono = false;
        return true;
    }
}

bool Plansza::czy_wygrana()
{
    if(ilosc_segmentow > 0) return false;
    else return true;
}

int Plansza::rozmiar_planszy()
{
    return rozmiar;
}

void Plansza::informacje()
{
    cout << "Calosciowy rozmiar statkow do zatopienia: " << ilosc_segmentow << endl;
}


#endif
