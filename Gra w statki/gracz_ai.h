/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#include <iostream>
#include <math.h>
#include <windows.h>
#include "plansza.h"

using namespace std;

class Gracz_AI
{
private:

    bool **tablica_strzalow; //tablica przechowuje miejsca, w ktore AI oddal strrzal (false - brak strzalu; true - wykonany strzal)
    int rozmiar; //rozmiar tablicy

    //zmienne wykorzystywane do podejmowania decyzji przez AI
    int trafX, trafY; //w zmiennych przechowywane sa wsp. jezeli strzal okazal sie celny
    int lewo_prawo, gora_dol; //zmienne okreslaja kierunek strzalu
    // lewo_prawo: 0-strzal w lewo, 1-strzal w prawo, 2-wartosc informujaca, ze sprawdzono juz 2 poprzednie opcje
    // gora_dol: 0-strzal w dol, 1-strzal w gore, 2-wartosc informujaca, ze sprawdzono juz 2 poprzednie opcje
    bool poprzedni_strzal; //zmienna przechowuje informacje czy poprzedni strzal byl trafiony
    bool czy_zatopiono; //zmienna przechowuje informacje czy poprzednio trafiony statek byl zatopiony

public:

    //metody publiczne:
    bool strzelaj(Plansza &p1); //funkja wykonuje strzal do planszy i informuje czy AI wygral
                                //IN: plansza przeciwnika
                                //OUT: false - nie wygral; true - wygral


    Gracz_AI(int rozm = 10) : rozmiar(rozm)
    {
        //tworzenie tablicy o podanym rozmiarze (wartosc domniemana 10)
        tablica_strzalow = new bool*[rozmiar];
        for(int i=0; i<rozmiar; i++)
            tablica_strzalow[i] = new bool[rozmiar];
        //ustawianie zmiennych w tablicy na wartosc false - brak strzalu
        for(int i=0; i<rozmiar; i++)
        {
            for(int j=0; j<rozmiar; j++)
                tablica_strzalow[i][j] = false;
        }
        //ustawianie poczatkowych wartosci zmiennych
        czy_zatopiono = poprzedni_strzal = false;
    }

    ~Gracz_AI()
    {
        //dealokacja pamieci
        for ( int i=0; i<rozmiar; ++i)
            delete [] tablica_strzalow[i];
        delete [] tablica_strzalow;
    }

};

bool Gracz_AI::strzelaj(Plansza &p1)
{
    int x,y;
    srand(time(NULL));

    cout << "[WROG]: ";
    /*petla sprawdza czy poprzedni strzal byl celny*/
    //jezeli nie byl celny:
    if(poprzedni_strzal == false)
    {
        //szukanie pola, w ktore nie wykonano jeszcze strzalu:
        do
        {
            x = rand()%rozmiar;
            y = rand()%rozmiar;
        }
        while (tablica_strzalow[x][y] == true);

        /*wykonanie strzalu*/
        //jezeli okazal sie celny:
        if(p1.strzal(x,y,czy_zatopiono))
        {
            gora_dol = lewo_prawo = 1; //ustawianie parametrow kolejnego strzalu
            tablica_strzalow[x][y] = true;
            poprzedni_strzal = true; //ustwianie zmiennej inf. o calnym strzale
            //ustawianie zmiennych przechowujacych inf. o wsp. celnego strzalu
            trafX = x;
            trafY = y;

            if(czy_zatopiono == false) strzelaj(p1); //jezeli statku nie zatopiono wykonuje kolejny strzal (nie losowy pkt.)
            else
            {
                //jezeli statek zatopiono wykonuje kolejny strzal (losowy pkt.)
                poprzedni_strzal = false;
                strzelaj(p1);
            }
        }
        //jezeli okazal sie niecelny
        else
        {
            tablica_strzalow[x][y] = true;
            poprzedni_strzal = false;
            return p1.czy_wygrana();
        }
    }
    //jezeli poprzedni strzal byl celny:
    else
    {
        /*Strzal w powyzej trafionego calu */
        //sprawdzenie czy pkt. powyzej istnieje (czy nie przekroczono rozmiaru tablicy)
        //sprawdzenie czy w pkt. powyzej juz wczesniej strzelano
        //sprawdzenie czy nalezy wykonac strzal powyzej celu
        if(trafX+1 < rozmiar && tablica_strzalow[trafX+1][trafY] == false && lewo_prawo == 1)
        {
            //Wykonanie strzalu:
            //Jezeli strzal okazal sie celny:
            if(p1.strzal(trafX+1,trafY,czy_zatopiono))
            {
                tablica_strzalow[trafX+1][trafY] = true; //aktualizacja tablicy strzalow
                trafX++; //ustawianie wsp. trafionego strzalu
                if(czy_zatopiono == false) strzelaj(p1); //jezeli nie zatopiono statku wykonaj kolejny strzal
                else //jezeli zatopiono:
                {
                    poprzedni_strzal = false; //ustawianie zmiennej o coelnym strzale na wartosc domyslna
                    return p1.czy_wygrana(); //zwracanie informacji czy AI wygral
                }
            }
            //Jezeli strzal okazal sie niecelny:
            else
            {
                tablica_strzalow[trafX+1][trafY] = true; //aktualizacja tablicy strzalow
                lewo_prawo = 0; //ustawianie kolejnego strzalu ponizej celu
                return p1.czy_wygrana(); //zwracanie informacji czy AI wygral
            }
        }

        /*Strzal w powyzej trafionego calu */
        //analogicznie do instrukcji powyzej
        //sprawdzenie czy nalezy wykonac strzal ponizej celu
        else if(trafX-1 > 0 && tablica_strzalow[trafX-1][trafY] == false && lewo_prawo == 0)
        {
            if(p1.strzal(trafX-1,trafY,czy_zatopiono))
            {
                tablica_strzalow[trafX-1][trafY] = true;
                trafX--;
                if(czy_zatopiono == false) strzelaj(p1);
                else
                {
                    poprzedni_strzal = false;
                    return p1.czy_wygrana();
                }
            }
            else
            {
                tablica_strzalow[trafX-1][trafY] = true;
                lewo_prawo = 2; //ustawienie zamiennej na informacje, ze pkt. po prawej i po lewej stronie zostaly sprawdzone
                return p1.czy_wygrana();
            }
        }
        /*Strzal w powyzej trafionego calu */
        //analogicznie do instrukcji powyzej
        //sprawdzenie czy nalezy wykonac strzal z prawej strony celu
        else if(trafY+1 < rozmiar && tablica_strzalow[trafX][trafY+1] == false && gora_dol == 1)
        {
            if(p1.strzal(trafX,trafY+1,czy_zatopiono))
            {
                tablica_strzalow[trafX][trafY+1] = true;
                trafY++;
                if(czy_zatopiono == false) strzelaj(p1);
                else
                {
                    poprzedni_strzal = false;
                    return p1.czy_wygrana();
                }
            }
            else
            {
                tablica_strzalow[trafX][trafY+1] = true;
                gora_dol = 0;
                return p1.czy_wygrana();
            }
        }
        /*Strzal w powyzej trafionego calu */
        //analogicznie do instrukcji powyzej
        //sprawdzenie czy nalezy wykonac strzal z lewej strony celu
        else if(trafY-1 > 0 && tablica_strzalow[trafX][trafY-1] == false && gora_dol == 0)
        {
            if(p1.strzal(trafX,trafY+1,czy_zatopiono))
            {
                tablica_strzalow[trafX][trafY-1] = true;
                trafY++;
                if(czy_zatopiono == false) strzelaj(p1);
                else
                {
                    poprzedni_strzal = false;
                    return p1.czy_wygrana();
                }
            }
            else
            {
                tablica_strzalow[trafX][trafY-1] = true;
                gora_dol = 2;
                return p1.czy_wygrana();
            }
        }
        poprzedni_strzal= false;
    }
}
