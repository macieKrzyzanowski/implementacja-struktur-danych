#include <iostream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include "kolejka_na_liscie.h"

using namespace std;

int main()
{
    Kolejka <int> k1; //alokacja stosu typu int
    srand(time(NULL));

    LARGE_INTEGER frequency;        // ticks per second
    LARGE_INTEGER t1, t2;           // ticks
    double elapsedTime;

    // get ticks per second
    QueryPerformanceFrequency(&frequency);

    // start timer
    QueryPerformanceCounter(&t1);

    for(int i=0; i<=500000; i++)
        k1.enqueue((rand()%10)+1);

    // stop timer
    QueryPerformanceCounter(&t2);

    // compute and print the elapsed time in millisec
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << elapsedTime << " ms.\n";

	return 0;
}
