#include <iostream>
#include "drzewoAVL.h"

using namespace std;

int main()
{
    int tablica[8] = {10,20,30,40,4,3,2,1};
    Drzewo drzewo;

    for(int i=0; i<8; i++)
    {
        cout << "Dodawanie elementu: " << tablica[i] <<": Rodzaje rotacji: ";
        drzewo.add(tablica[i]);
        cout << endl;
    }

    cout << "\n-----------------  Zawartosc drzewa:  -----------------" << endl;
    for(int i=0; i<8; i++)
        drzewo.printInRoot(tablica[i]);

    cout << "\nWysokosc drzewa: " << drzewo.height();
    cout << "\nPrzejscie InOrder: ";
    drzewo.printInOrder();
    cout << "\nPrzejscie PostOrder: ";
    drzewo.printPostOrder();
    cout << "\nPrzejscie PreOrder: ";
    drzewo.printPreOrder();

    cout << "\n\nUsuwanie elementu: " << tablica[0] <<": Rodzaje rotacji: ";
    drzewo.remove(tablica[0]);
    cout << "\nUsuwanie elementu: " << tablica[1] <<": Rodzaje rotacji: ";
    drzewo.remove(tablica[1]);

    cout << endl;

    cout << "\n-----------------  Zawartosc drzewa:  -----------------" << endl;
    for(int i=0; i<8; i++)
        drzewo.printInRoot(tablica[i]);




    return 0;
}
