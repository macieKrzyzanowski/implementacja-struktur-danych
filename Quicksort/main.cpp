#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include <fstream>
#include <deque>

using namespace std;

template <typename T>
void inPlaceQuickSort(T tablica[], int lewy, int prawy)
{
    T pivot = tablica[lewy]; //wybieranie elementu osiowego
    int l = lewy, r = prawy;
    T wsk;

    while(l <= r)
    {
        while(tablica[l] < pivot)
            l++;
        while(tablica[r] > pivot)
            r--;

        if(l <= r)
        {
            wsk = tablica[l];
            tablica[l++] = tablica[r];
            tablica[r--] = wsk;
        }
    }

    if(r > lewy)
        inPlaceQuickSort(tablica,lewy, r);
    if(l < prawy)
        inPlaceQuickSort(tablica, l, prawy);
}


int main()
{
    fstream plik;
    plik.open("plik_tekstowy.txt",ios::out);

    LARGE_INTEGER frequency;        // ticks per second
    LARGE_INTEGER t1, t2;           // ticks
    double elapsedTime;

    int ile = 500000; //ilosc elementow do pososrotwania
    int proce = ile * 0; //ilosc pososrotwanych element�w

    //dynamiczna alokacja tablicy
    int *tablica=new int [ile];
    int *bufor = new int [ile];

    //inicjowanie generatora
    srand(time(NULL));

   for(int i=0; i<100; i++)
    {
        //sortowanie czesci tablic
        for(int j=0; j<proce; j++)
            tablica[j] = rand()%1000000+1;

        inPlaceQuickSort(tablica, 0, proce-1);

        for(int k=proce; k<ile; k++)
            tablica[k] = rand()%1000000+1;

        // get ticks per second
        QueryPerformanceFrequency(&frequency);

        // start timer
        QueryPerformanceCounter(&t1);

        /////////////////////////////////////////////////////////////////////////////////////////////
        inPlaceQuickSort(tablica, 0, ile-1);
        /////////////////////////////////////////////////////////////////////////////////////////////

        // stop timer
        QueryPerformanceCounter(&t2);

        // compute and print the elapsed time in millisec
        elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
        cout << elapsedTime << " ms.\n";

         //prawdzenie poprawnosci posortowania
        int licznik = 1;
        while(licznik < ile)
        {
            if(tablica[licznik-1] > tablica[licznik])
            {
                 cout << "W sortowaniu wystapil blad." << endl;
                 cout << licznik;
            }
            licznik++;
        }

        plik << elapsedTime << endl;

    }

    plik.close();

    return 0;
}
