#include <iostream>
#include <cstdlib>
#include <ctime>

#include "tablica_haszujaca_linkowanie.h"

using namespace std;

//Funkcja sprawdza czy podana liczba wystepuje w tablicy
//IN: liczba - liczba jaka wylosowalismy
//    tab[] - tablica wylosowanych liczb
//    ile - rozmiar tablicy tab[]
//OUT: true jesli liczba wystepuje
//     false jesli liczba nie wystepuje
bool czyWystepuje(int liczba, int tab[], int ile)
{
    int i = 0; //licznik
    if(ile <= 0)
         return false;
    do
    {
        if( tab[ i ] == liczba )
             return true;
        else i++;
    } while(i < ile);

    return false;
}

//Funkcja losuje liczby bez powtorzen, z przedzialu od 1 do "przedzial"
//IN: tab[] - tablica dla liczb
//    przedzial - do jakiej wartosci wylosowac
//    ile - ilosc elemento do wylosowania
//OUT: tablica tab[] elementow wylosowanych bez powtorzen
void wylosuj(int tab[], int ile, int przedzial)
{
    srand(time(NULL));

    int wylosowanych = 0;
    do
    {
        int liczba = (rand()%przedzial) + 1;
        if(czyWystepuje(liczba,tab,wylosowanych) == false)
        {
            tab[wylosowanych] = liczba;
            wylosowanych++;
        }
    } while (wylosowanych < ile);
}

//Fukcja wyswietla zawartosc tablicy
//IN: tab[] - tablica, ktora wyswietla
//    ile - ilosc elementow tablicy
void wyswiel(int tab[], int ile)
{
    for(int i=0; i<ile; i++)
        cout << tab[i] << " ";
}




int main()
{

    //Parametry do ustawiania:
    int ilosc_liczb = 7;
    int przedzial_liczb = 200;

    int *wylosowane_liczby = new int [ilosc_liczb];
    wylosuj(wylosowane_liczby,ilosc_liczb,przedzial_liczb);
    cout << "Wylosowane liczby:\n";
    wyswiel(wylosowane_liczby,ilosc_liczb);
    cout << endl << endl;

    THaszujaca tab_hasz(ilosc_liczb);
    try
    {
        for(int i=0; i<ilosc_liczb; i++)
        {
            cout << "Dodawanie " << i+1 << " elementu.\n";
            tab_hasz.put(wylosowane_liczby[i],wylosowane_liczby[i]);
        }

        cout << "\n\nIlosc elementow przed usunieciem: " << tab_hasz.size() << endl;
        cout << "Usuniecie elementu " << wylosowane_liczby[3] << endl;
        tab_hasz.remove(wylosowane_liczby[3]);
        cout << "Usuniecie elementu " << wylosowane_liczby[1] << endl;
        tab_hasz.remove(wylosowane_liczby[1]);
        cout << "Ilosc elementow po usunieciu: " << tab_hasz.size() << endl << endl;

        cout << "Dodanie elementu o kluczu 1 i wartosci 50.\n";
        tab_hasz.put(1,50);

        cout << "\nWyszukanie elementu o kluczu 1.\n";
        int liczba = tab_hasz.get(1);
        cout << "\nElement o kluczu 1 ma wartosc " << liczba << endl;
    }
    catch (exception & e)
    {
		cerr << e.what() << endl;
	}



    return 0;
}
