#include <iostream>

using namespace std;

template <typename Object>
class Stos
{
private:
    int MAX_SIZE; //pojemnosc stosu
    int SIZE; //indeks na pierwsze puste pole stosu
    Object *S; //tablica stosu

public:
    Stos(); //konstruktor tablic
    ~Stos(); //destruktor tablicy

    void push(const Object & nowy); //dodaje element pusty(element)
    Object pop(); //usuwa element i zwraca element z wierzchu stosu
    Object & top() const; // zwraca ostatni element umieszcony na stosie
    int size() const; //zwraca ilosc przechowywanych elementow
    bool isEmpty() const; // sprawdza czy stos jest pusty
};


//---------------------Metody--------------------

template <typename Object>
Stos<Object>::Stos()
{
    MAX_SIZE = 1;
    S = new Object[MAX_SIZE];
    SIZE = 0;
}

template <typename Object>
Stos<Object>::~Stos()
{
    delete [] S;
}




template <typename Object>
void Stos<Object>::push(const Object & nowy)
{
    if(SIZE == MAX_SIZE - 1)
    {
        ++MAX_SIZE;
        Object *nowyS = new Object[MAX_SIZE];
        for(int i=0; i<=SIZE; i++)
            nowyS[i] = S[i];
        delete[] S;
        S = nowyS;
    }
        S[++SIZE] = nowy;
}

template <typename Object>
Object Stos<Object>::pop()
{
    if(isEmpty()) cerr << "Stos jest PUSTY" << endl;
    else return S[--SIZE];
}

template <typename Object>
Object & Stos<Object>::top() const
{
    if(isEmpty()) cerr << "Stos jest PUSTY" << endl;
    else return S[SIZE-1];
}

template <typename Object>
int Stos<Object>::size() const
{
    return SIZE;
}

template <typename Object>
bool Stos<Object>::isEmpty() const
{
    return (SIZE==0);
}

