#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;

template <typename T>
class Lista;

template <typename T>
class Kolejka;

template <typename T>
class wezel
{
private:
    T elem;
	wezel<T>* next;
	friend class Lista<T>;
public:
    T getElement() {return elem;} //metoda dostapu do wartosci elementu
    wezel getNext() {return next;} //metoda dostepu do nastepnego elementu
    void setElement(T nowyE) {elem = nowyE;} //metoda modyfikujaca element
    void setNext(wezel<T> nowyN) {next = nowyN;} //metoda modyfikuja nastepny
};

template <typename T>
class Lista
{
private:
    wezel<T> *head;
	wezel<T>* tail;
	int rozmiar;

protected:
    bool empty() const; //zwraca true gdy lista jest pusta lub false gdy posiada jakies elementy
    T front(); //zwraca wartosc pierwszego elementu
    void addFront(const T& e); //dodaje element na poczatku listy
    void removeFront(); //usuwa element z poczatku listy
    void addBack(const T& e); //dodaje element na koniec listy
    int size(); //zwraca rozmiar listy
    public: void show() const; //wyswietla zawartosc listy
    Lista() {head = NULL; tail = NULL; rozmiar = 0;}; //konstruktor
   ~Lista() //destruktor
   	{
		wezel <T> * wsk;
		while (head != NULL)
        {
			wsk = head;
			head = head->next;
			rozmiar--;
			delete wsk;
        }
   	}

};

//---------------------------------metody-----------------------------------

template <typename T>
bool Lista<T>::empty() const
{
    if (head == NULL) return true;
    else return false;
}

template <typename T>
int Lista<T>::size()
{
    return rozmiar;
}

template <typename T>
T Lista<T>::front()
{
    return head->elem;
}

template <typename T>
void Lista<T>::addFront(const T &e)
{
    wezel<T>* wsk = new wezel<T>;
    wsk->elem = e;
    wsk->next = head;
    if(empty())
    {
        head = wsk;
        tail = wsk;
    }
    else head = wsk;
    rozmiar++;
}

template <typename T>
void Lista<T>::removeFront()
{
    if(empty()) cerr << "Lista jest pusta";
    wezel<T>* old = head;
    head = old->next;
    delete old;
    rozmiar--;
}

template <typename T>
void Lista<T>::addBack(const T &e)
{
    wezel<T>* wsk = new wezel<T>;
    wsk->elem = e;
    wsk->next = NULL;
    if(empty())
    {
        head = wsk;
        tail = wsk;
    }
    else tail = wsk;
    rozmiar++;
}


//klasa dziedziczona z listy:

template <typename T>
class Stos:public Lista<T>
{
public:
    Stos() : Lista<T>::Lista() {}
    void push(T nowy) {Lista<T>::addFront(nowy);} //dodaje element push(element)
    T pop() //usuwa element i zwraca element z wierzchu stosu
    {
        T usuwany = Lista<T>::front();
        Lista<T>::removeFront();
        return usuwany;
    }
    T topS() {return Lista<T>::front();} // zwraca ostatni element umieszcony na stosie
    int sizeS() {return Lista<T>::size();} //zwraca ilosc przechowywanych elementow
    bool isEmpty() {return Lista<T>::empty();}; // sprawdza czy stos jest pusty
};
