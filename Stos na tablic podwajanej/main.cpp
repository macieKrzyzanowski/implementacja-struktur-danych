#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include "stos_tab_podw.h"

using namespace std;

template <typename Object>
void wyswietl(Stos <Object> s1)
{
    cout << "---------- STOS ----------" << endl;
    for(int i = s1.size(); i>0; i--)
        cout << s1.pop() << " ";
    cout << endl << "--------------------------" << endl;
}



int main()
{
    Stos <int> s1; //alokacja stosu typu int
    srand(time(NULL));

    LARGE_INTEGER frequency;        // ticks per second
    LARGE_INTEGER t1, t2;           // ticks
    double elapsedTime;

    // get ticks per second
    QueryPerformanceFrequency(&frequency);

    // start timer
    QueryPerformanceCounter(&t1);

    for(int i=0; i<=1000; i++)
        s1.push((rand()%10)+1);

    // stop timer
    QueryPerformanceCounter(&t2);

    // compute and print the elapsed time in millisec
    elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
    cout << elapsedTime << " ms.\n";

	return 0;
}

