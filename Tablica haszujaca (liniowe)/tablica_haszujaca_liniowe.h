#include <iostream>
#include <math.h>
#include <exception>

using namespace std;

class THaszujaca;

/* obsluga wyjatkow */
class DontExistException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Element o podanym kluczu nie istnieje.";
	}
};

class IsFullException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Tablica jest pelna.";
	}
};

//////////////////////////////////////////////////////////

class Element
{
private:
    int wartosc;
    int klucz;
    friend class THaszujaca;

public:
    element()
    {
        wartosc = 0;
        klucz = 0;
    }
};

class THaszujaca
{
private:
    Element *tablica;
    int ilosc_elementow;
    int ilosc_dodanych_elementow;

    bool czy_pierwsza(int); //metoda sprawdzaczy podana liczba jest pierwsza IN: liczba OUT: true - jesli liczba pierwsza; false - jesli liczba nie jest pierwsza
    int hasz(int); //metoda haszujaca zamieniajaca klucze na indeksy tablicy IN: klucz dodanego elementu OUT: wartosc funcki haszujacej

public:
    THaszujaca(int); //konstruktor alokujacy tablice (domyslnie 101 elementow)
    ~THaszujaca() {delete [] tablica;} //destruktor dealokujacy tablice

    int get(int); //metoda wyszukuje element o podanym kluczu i zwraca jego wartosc IN: klucz OUT: wartosc
    void put(int,int); //metoda dodaje element o podanym kluczu i wartosc IN: klucz, wartosc
    int remove(int); //metoda usuwa element o podanym kluczu IN: klucz
    int size(); //metoda zwraca ilosc dodanych elementow do tablicy haszujacej OUT: ilosc elementow
    bool isEmpty(); //metoda sprawdza czy tablica haszujaca jest pusta OUT: true - jesli pusta; false - jesli nie jest pusta
};

THaszujaca::THaszujaca(int ile = 101)
{
    if(!czy_pierwsza(ile))
    {
        cerr << "Podana liczna nie jest pierwsza.";
        ile = 101;
    }
    ilosc_elementow = ile;
    tablica = new Element [ile];

    for(int i=0; i<ilosc_elementow; i++)
    {
        tablica[i].wartosc = 0;
        tablica[i].klucz = NULL;
    }

    ilosc_dodanych_elementow = 0;
}

int THaszujaca::hasz(int klucz)
{
    return (klucz%ilosc_elementow);
}

int THaszujaca::size()
{
    return ilosc_dodanych_elementow;
}

bool THaszujaca::isEmpty()
{
    return !ilosc_dodanych_elementow;
}

int THaszujaca::get(int klucz)
{
    int i = hasz(klucz); //zmienna przechowyje wartosc haszujaca
    int ilosc_probek = 1; //zmienna przechoujeilosc przebanadnych probek

    while(ilosc_probek < ilosc_elementow)
    {
        if(tablica[i].klucz == klucz)
        {
            cout << "Probki: " << ilosc_probek << endl;
            return tablica[i].wartosc;
        }
        else if(tablica[i].klucz == NULL)
        {
            cout << "Probki: " << ilosc_probek << endl;
            throw DontExistException();
        }
        else
            i = (i+1)%ilosc_elementow;

        ilosc_probek++;
    }

    cout << "Probki: " << ilosc_probek << endl;
    throw DontExistException();
}

void THaszujaca::put(int klucz,int wart)
{
    int i = hasz(klucz); //zmienna przechowyje wartosc haszujaca
    int ilosc_probek = 1; //zmienna przechoujeilosc przebanadnych probek

    if(ilosc_elementow < ilosc_dodanych_elementow) throw IsFullException(); //metoda rzuca wyjatek kiedy tablica jest pelna

    while(ilosc_probek < ilosc_elementow)
    {
        if(tablica[i].klucz == NULL)
        {
            tablica[i].wartosc = wart;
            tablica[i].klucz = klucz;
            cout << "Probki: " << ilosc_probek << endl;
                 ++ilosc_dodanych_elementow;
                 break;
        }

        else i = (i+1)%ilosc_elementow;

        ilosc_probek++;
    }
}

int THaszujaca::remove(int klucz)
{
    int i = hasz(klucz); //zmienna przechowyje wartosc funkcji haszujacej
    int ilosc_probek = 1;

    while(ilosc_probek < ilosc_elementow)
    {
        if(tablica[i].klucz == klucz)
        {
            int wart = tablica[i].wartosc;
            tablica[i].wartosc = NULL;
            tablica[i].klucz = NULL;
            cout << "Probki: " << ilosc_probek << endl;
                 --ilosc_dodanych_elementow;
            return wart;

        }
        else i = (i+1)%ilosc_elementow;
    }
}

bool THaszujaca::czy_pierwsza(int liczba)
{
    //przedzal w jakim sprawdzamy dana liczbe
    int prawy = sqrt(liczba);
    int lewy = 2;

    while (lewy <= prawy)
        if(!(liczba%lewy++))
            return false;

    return true;
}




