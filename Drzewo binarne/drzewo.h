#include <iostream>
#include <exception>

using namespace std;

class DontExistException;
class HasDescendantsException;
class AlreadyHasSonsException;

template <typename Typ>
class ElementDrzewa
{
private:
    Typ wartosc;
    ElementDrzewa *ojciec;
    ElementDrzewa *lewy_syn;
    ElementDrzewa *prawy_syn;

public:

    /*********************** Metody dostepu **************************/
    Typ getWartosc() const {return wartosc;}
    ElementDrzewa *getOjciec() const {return ojciec;}
    ElementDrzewa *getLewy_syn() const {return lewy_syn;}
    ElementDrzewa *getPrawy_syn() const {return prawy_syn;}
    void setWartosc(Typ nowyW) {wartosc = nowyW;}
    void setOjciec(ElementDrzewa *nowyO) {ojciec = nowyO;}
    void setLewy_syn(ElementDrzewa *nowyL) {lewy_syn = nowyL;}
    void setPrawy_syn(ElementDrzewa *nowyP) {prawy_syn = nowyP;}


    ElementDrzewa* visit(int index) //metoda zwraca syna o podanym indeksie
    {
        if(index == 0) return lewy_syn;
        else if(index == 1) return prawy_syn;
        else throw DontExistException();
    }

    ElementDrzewa()
    {
        wartosc = 0;
        ojciec = NULL;
        lewy_syn = NULL;
        prawy_syn = NULL;
    }
};


template <typename Typ>
class DrzewoBinarne: public ElementDrzewa<Typ>
{
private:
    ElementDrzewa<Typ> *korzen;
    int iloscElementow;

protected:

    /*********************** Metody dostepu **************************/
    ElementDrzewa<Typ> *getKorzen() const {return korzen;}
    int getIloscElementow() const {return iloscElementow;}
    void setKorzen(ElementDrzewa<Typ> *nowyK) {korzen = nowyK;}
    void setIloscElementow(int nowyI) {iloscElementow = nowyI;}

public:
    DrzewoBinarne()
    {
        korzen = NULL;
        iloscElementow = 0;
    }

    ElementDrzewa<Typ>* root() {return korzen;} //metoda zwraca wskaznik na korzen
    void addRoot(Typ); //dadaje nowy korzen
    void add(Typ,ElementDrzewa<Typ>*); //metoda dodaje element do drzewa o (o wartosc,do ojca)
    void remove(ElementDrzewa<Typ>*); //metoda usuwa wskazany element
    bool isEmpty() {return !iloscElementow;} //metoda sprawdza czy drzewo jest puste
    int size() {return iloscElementow;} //metoda zwraca ilosc elementow
    void printInElement(ElementDrzewa<Typ>*); //metoda podaje wartosc ojca i potomkow
};

template <typename Typ>
void DrzewoBinarne<Typ>::addRoot(Typ wart)
{
	ElementDrzewa<Typ>* nowy = new ElementDrzewa<Typ>;
	if(korzen == NULL)
    {
        nowy->setOjciec(NULL);
        nowy->setWartosc(wart);
        korzen = nowy;
        iloscElementow++;
    }
    else cerr << "Korzen juz istnieje." << endl;
}

template <typename Typ>
void DrzewoBinarne<Typ>::add(Typ wart, ElementDrzewa<Typ> *wsk)
{
	ElementDrzewa<Typ> *nowy = new ElementDrzewa<Typ>;
	if (korzen == NULL) addRoot(wart);
	else
    {
        nowy->setWartosc(wart);
		nowy->setOjciec(wsk);
		if(wsk->getLewy_syn() == NULL) wsk->setLewy_syn(nowy);
		else if(wsk->getPrawy_syn() == NULL) wsk->setPrawy_syn(nowy);
		else throw AlreadyHasSonsException();
		iloscElementow++;
	}
}

template <typename Typ>
void DrzewoBinarne<Typ>::remove(ElementDrzewa<Typ>* wsk)
{
	ElementDrzewa<Typ> *przodek = wsk->getOjciec();
	if ((wsk->getLewy_syn() != NULL) && (wsk->getPrawy_syn() != NULL)) throw HasDescendantsException();
	else
    {
        if(przodek->getLewy_syn() == wsk)
        {
            przodek->setLewy_syn(NULL);
            wsk->setOjciec(NULL);
            delete wsk;
        }
        else
        {
            przodek->setPrawy_syn(NULL);
            wsk->setOjciec(NULL);
            delete wsk;
        }
	}
	iloscElementow--;
}

template<typename Typ>
void DrzewoBinarne<Typ>::printInElement(ElementDrzewa<Typ>* wsk)
{
    cout << "Ojciec: " << wsk->getWartosc() << endl;
    cout << "-------------------------" <<endl;
    if(wsk->getLewy_syn() == NULL) cout << "Lewy syn = NULL" << endl;
    else cout << "Lewy syn: " << wsk->getLewy_syn()->getWartosc() << endl;
    if(wsk->getPrawy_syn() == NULL) cout << "Prawy syn = NULL" << endl;
    else cout << "Prawy syn: " << wsk->getPrawy_syn()->getWartosc() << endl;
    cout << "-------------------------" << endl;
}

//Metoda zwraca wysykosc grzewa tree o wsk. na korzen
template <typename Typ>
int height(const DrzewoBinarne<Typ> &tree, ElementDrzewa<Typ>* wsk)
{
    if (wsk == NULL) return 0;
    else return max(height(tree,wsk->getLewy_syn()), height(tree,wsk->getPrawy_syn())) + 1;
}

//procedura przejscia PreOrder
template <typename Typ>
void printPreOrder(const DrzewoBinarne<Typ> &tree, ElementDrzewa<Typ>* wsk)
{
    if(wsk != NULL)
    {
        cout << wsk->getWartosc() << " ";
        if(wsk->getLewy_syn() != NULL) printPreOrder(tree,wsk->getLewy_syn());
        if(wsk->getPrawy_syn() != NULL) printPreOrder(tree,wsk->getLewy_syn());
    }
    else cout << "Drzewo jest puste." << endl;
}

//procedura przejscia InOrder
template <typename Typ>
void printInOrder(const DrzewoBinarne<Typ> &tree, ElementDrzewa<Typ>* wsk)
{
    if(wsk != NULL)
    {
        if(wsk->getLewy_syn() != NULL) printInOrder(tree,wsk->getLewy_syn());
        cout << wsk->getWartosc() << " ";
        if(wsk->getPrawy_syn() != NULL) printInOrder(tree,wsk->getPrawy_syn());
    }
    else cout << "Drzewo jest puste." << endl;
}

//procedura przejscia PostOrder
template <typename Typ>
void printPostOrder(const DrzewoBinarne<Typ> &tree, ElementDrzewa<Typ>* wsk)
{
    if(wsk != NULL)
    {
        if(wsk->getLewy_syn() != NULL) printPostOrder(tree,wsk->getLewy_syn());
        if(wsk->getPrawy_syn() != NULL) printPostOrder(tree,wsk->getPrawy_syn());
        cout << wsk->getWartosc() << " ";
    }
    else cout << "Drzewo jest puste." << endl;
}


class DontExistException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Podany element drzewa nie istnieje.";
	}
};

class HasDescendantsException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Element ma potomkow.";
	}
};

class AlreadyHasSonsException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Nie mozna dodac elementu. Element ma juz dwojke synow.";
	}
};
