/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#include <iostream>
#include <time.h>
#include <cstdlib>
#include <windows.h>
#include <fstream>
#include <exception>
#include "graf_macierz_sasiedztwa.h"

using namespace std;

/****************** DFS *******************/
///////////////////////////////////////////////////////////////////////////////

//funkcja ustawia wszystkie etykiety elemetow grafu na false oraz wywoluje druga czesc algorytmu
//IN: graf
template <typename Typ>
void DFS(Graph<Typ> &G)
{
    int i;
    for(i=0; i<G.sizeVert(); i++)
        G.vertices(i)->setLabel(false);
    for(i=0; i<G.sizeEdge(); i++)
        G.edges(i)->setLabel(false);
    for(int i=0; i<G.sizeVert(); i++)
    {
        if(G.vertices(i)->getLabel() == false)
            DFS(G,G.vertices(i));
    }
}

//fukncja wykonuje przejcie grafu w glab
//IN: graf, wierzcholek
template <typename Typ>
void DFS(Graph<Typ> &G, Vertex<Typ>* v)
{
    v->setLabel(true);
    for(int i=0; i < G.sizeIncident(v); i++)
    {
        Edge<Typ>* e = G.incidentEdges(v,i);
        if(e->getLabel() == false)
        {
            Vertex<Typ>* w = G.opposite(v,e);
            if(w->getLabel() == false)
            {
                e->setLabel(true);
                DFS(G,w);
            }
            else
                e->setLabel(-1);
        }
    }
}
///////////////////////////////////////////////////////////////////////////////


/****************** BFS *******************/
///////////////////////////////////////////////////////////////////////////////

//funkcja ustawia wszystkie etykiety elemetow grafu na false oraz wywoluje druga czesc algorytmu
//IN: graf
template <typename Typ>
void BFS(Graph<Typ> &G)
{
    int i;
    for(i=0; i<G.sizeVert(); i++)
        G.vertices(i)->setLabel(false);
    for(i=0; i<G.sizeEdge(); i++)
        G.edges(i)->setLabel(false);
    for(i=0; i<G.sizeVert(); i++)
    {
        if(G.vertices(i)->getLabel() == false)
            BFS(G,G.vertices(i));
    }
}

//fukncja wykonuje przejcie grafu wszerz
//IN: graf, wierzcholek
template <typename Typ>
void BFS(Graph<Typ> &G, Vertex<Typ>* s)
{
    Sekwencja<Vertex<Typ>*> L;
    L.addBack(s);
    s->setLabel(true);

    while(!L.isEmpty())
    {
        Vertex<Typ>* v = L.front();
        L.removeFront();
        for(int k=0; k<G.sizeIncident(v); k++)
        {
            Edge<Typ>* e = G.incidentEdges(v,k);
            if(e->getLabel() == false)
            {
                Vertex<Typ>* w = G.opposite(v,e);
                if(w->getLabel() == false)
                {
                    e->setLabel(true);
                    w->setLabel(true);
                    L.addBack(w);
                }
                else e->setLabel(-1);
            }
        }
    }
}
///////////////////////////////////////////////////////////////////////////////

//funkcja losuje tablicice losowych elementow o podanej ilosci i w podanym przedziale
// IN: tab[] - tablica losowych elemento
//     ile - ilosc elementow
//     przedzial - przedzial w jakim losujemy liczby od 0 do ...
void wylosuj(int tab[], int ile, int przedzial)
{
    srand(time(NULL));

    int wylosowanych = 0;
    do
    {
        int liczba = (rand()%przedzial) + 1;
        tab[wylosowanych] = liczba;
        wylosowanych++;
    } while (wylosowanych <= ile);
}

//funkcja generuje losowe liczby, oraz wypelnia nimi graf
//IN: graf, ilosc wierzcholkow, gestosc grafu
void generowanie_grafu(Graph<int> &G, int ilosc_wierzcholkow, float gestosc_grafu)
{
    int ilosc_krawedzi = ilosc_wierzcholkow*(ilosc_wierzcholkow-1)*gestosc_grafu*0.5; //zmienna przechowuje ilosc krawedzi
    int tab_wierz[ilosc_wierzcholkow];
    int tab_kraw[ilosc_krawedzi];
    wylosuj(tab_wierz,ilosc_wierzcholkow,ilosc_wierzcholkow);
    wylosuj(tab_kraw,ilosc_krawedzi,ilosc_krawedzi);

    //dodawanie wierzcholkow
    for(int i=0; i<ilosc_wierzcholkow; i++)
        G.insertVert(tab_wierz[i]);

    int k = 0;
    int i = 0;
    //petla laczy wierzcholki krawedziami w celu uzyskania zadanej gestosci grafu
    while(k < ilosc_krawedzi)
    {
        for(int j=i+1; j<ilosc_wierzcholkow; j++)
        {
            if(k < ilosc_krawedzi)
            {
                G.insertEdge(tab_wierz[i],tab_wierz[j],tab_kraw[k]);
                k++;
            }
            else break;
        }
        i++;
    }
}

//funkja umozliwia zapis struktury grafu do pliku txt
template <typename Typ>
void zapisz_graf(Graph<Typ> G)
{
    fstream plik;
    plik.open("zapisany_graf.txt",ios::out);

    plik << G.sizeVert() << endl; //w pierwszej linijce zapisujemy liczbe wierzcholkow
    plik << G.sizeEdge() << endl; //w drugiej linijce zapisujemy liczbe krawedzi

    for(int i=0; i<G.sizeVert(); i++)
        plik << G.vertices(i)->getWartosc() << " ";

    plik << endl;

    for(int i=0; i<G.sizeEdge(); i++)
    {

        plik << G.edges(i)->getPoczatek()->getWartosc() << " ";
        plik << G.edges(i)->getKoniec()->getWartosc() << " ";
        plik << G.edges(i)->getWartosc();
        plik << endl;
    }

    plik.close();
}

//funkja umozliwia wczytanie struktury grafu z pliku txt
template <typename Typ>
void wczytaj_graf(Graph<Typ> &G)
{
    G.clear();

    ifstream plik;
    plik.open("zapisany_graf.txt",ios::in);

    int ilosc_wierzcholkow, ilosc_krawedzi;
    plik >> ilosc_wierzcholkow;
    plik >> ilosc_krawedzi;

    int tab_wierz[ilosc_wierzcholkow];
    int tab_kraw[3][ilosc_krawedzi];

    for(int i=0; i<ilosc_wierzcholkow; i++)
        plik >> tab_wierz[i];

    for(int i=0; i<ilosc_wierzcholkow; i++)
        G.insertVert(tab_wierz[i]);

    for(int i=0; i<ilosc_krawedzi; i++)
    {
        plik >> tab_kraw[0][i];
        plik >> tab_kraw[1][i];
        plik >> tab_kraw[2][i];
    }

    for(int i=0; i<ilosc_krawedzi; i++)
        G.insertEdge(tab_kraw[0][i],tab_kraw[1][i],tab_kraw[2][i]);

    plik.close();
}

int main()
{
    // ustawianie parametrow pomiaru:
    int ilosc_wierzcholkow = 1000;
    float gestosc = 1;
    Graph<int> graf(ilosc_wierzcholkow);

    try
    {
        fstream plik1,plik2;
        plik1.open("pomiaryDFS.txt",ios::out);
        plik2.open("pomiaryBFS.txt",ios::out);

        for(int i=0; i<100; i++)
        {
            generowanie_grafu(graf,ilosc_wierzcholkow,gestosc);
            cout << i+1 << "." << endl;
            LARGE_INTEGER frequency;        // ticks per second
            LARGE_INTEGER t1, t2;           // ticks
            double elapsedTime;

            // get ticks per second
            QueryPerformanceFrequency(&frequency);

            // start timer
            QueryPerformanceCounter(&t1);

            /////////////////////////////////////////////////////////////////////////////////////////////

            DFS(graf);

            /////////////////////////////////////////////////////////////////////////////////////////////

            // stop timer
            QueryPerformanceCounter(&t2);

            // compute and print the elapsed time in millisec
            elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
            cout << "DFS: ";
            cout << elapsedTime << " ms.\n";

            plik1 << elapsedTime << endl;

            /*********************************************/

            // get ticks per second
            QueryPerformanceFrequency(&frequency);

            // start timer
            QueryPerformanceCounter(&t1);

            /////////////////////////////////////////////////////////////////////////////////////////////

            BFS(graf);

            /////////////////////////////////////////////////////////////////////////////////////////////

            // stop timer
            QueryPerformanceCounter(&t2);

            // compute and print the elapsed time in millisec
            elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
            cout << "BFS: ";
            cout << elapsedTime << " ms.\n";

            plik2 << elapsedTime << endl;

            graf.clear();
        }
        plik1.close();
        plik2.close();
    }
    catch (exception & e)
    {
        cerr << e.what() << endl;
    }


    return 0;
}
