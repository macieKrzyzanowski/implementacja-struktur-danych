/*
Zajecia: Projektowanie algorytmow i metody sztucznej inteligencji
Termin labor.: WT 10:00-12:00
Wykonal: Maciej Krzyzanowski (226505)
*/

#include <iostream>
#include <exception>
#include "sekwencja.h"

using namespace std;

class DontExistException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Podany element nie istnieje.";
	}
};

template <typename Typ> class Graph;
template <typename Typ> class Vertex;
template <typename Typ> class Edge;

/////////////////////////////////////////////////////////////////////////////////

template <typename Typ>
class Edge
{
private:
    Typ wartosc; //wartosc krawedzi
    Vertex<Typ>* poczatek; //referencja do wierzcholka poczatkowego
    Vertex<Typ>* koniec; //referencja do wierzcholka koncowego
    Edge* pozycja; //referencja do pozycji na liscie krawedzi

    int label; //etukieta wykorzystywane podczas trawersacji grafu (wartosc: -1(krawedz powrotna), 0(krawedzi nieodwiedzona), 1(krawedz odwiedzona))
public:
    Edge()
    {
        poczatek = koniec = NULL;
        pozycja = NULL;
        label = 0;
    }
    ~Edge()
    {
        delete pozycja;
        delete koniec;
        delete poczatek;
    }
    int getLabel() {return label;}
    void setLabel(int lab) {label=lab;}
    Typ getWartosc() {return wartosc;}
    Vertex<Typ>* getPoczatek() {return poczatek;}
    Vertex<Typ>* getKoniec() {return koniec;}

    friend class Vertex<Typ>;
    friend class Graph<Typ>;
};

/////////////////////////////////////////////////////////////////////////////////

template <typename Typ>
class Vertex
{
private:
    Typ wartosc; //wartosc wierzcholka
    int klucz; //zamienna przechowuje klucz wierzcholka (indeks w macierzy sasiedztwa)
    Vertex* pozycja; //referencja do polozenia na liscie krawedzi

    bool label; //etukieta wykorzystywane podczas trawersacji grafu (wartosci: false-wierzcholek nieodwiedzony; true-wierzcholek odwiedzony)
    static int licznik; //zmienna przechowuje ilosc utworzonych wierzcholkow

public:
    Vertex()
    {
        klucz = licznik;
        licznik++;
        label = 0;
    }
    ~Vertex()
    {
        delete pozycja;
    }
    bool getLabel() {return label;}
    void setLabel(bool lab) {label=lab;}
    Typ getWartosc() {return wartosc;}
    void zmniejszLicznik() {licznik--;}
    void wyzerujLicznik() {licznik = 0;}
    void getLicz() {cout << licznik;}

    friend class Edge<Typ>;
    friend class Graph<Typ>;
};

template <typename Typ>
int Vertex<Typ>::licznik = 0; //inicjalizacja zmiennaj statycznej wartoscia 0

/////////////////////////////////////////////////////////////////////////////////

template <typename Typ>
class Graph
{
private:
    Edge<Typ> ***Macierz; //dwuwymiarowa macierz sasiedztwa wskaznikow na krawedzie
    int rozmiar;

    Sekwencja<Vertex<Typ>*> V; //lista wskaznikow na wierzcholki
    Sekwencja<Edge<Typ>*> E; //lista wskaznikow na krawedzie

    Vertex<Typ>* findVert(Typ); //metoda wyszukuje wierzcholek na podstawie podanej wartosci IN: wartosc wierz. OUT: wsk. na wierzcholek
    Edge<Typ>* findEdge(Typ); //metoda wyszukuje krawedz na podstawie podanej wartosci IN: wartosc kraw. OUT: wsk na krawedz

public:

    Graph(int ile = 0) //konstruktor z domniemana wartoscia
    {
        rozmiar = ile;
        Macierz = new Edge<Typ>**[ile];
        for(int i=0; i<ile; ++i)
            Macierz[i] = new Edge<Typ>*[ile];

        for(int i=0; i<rozmiar; ++i)
            for(int j=0; j<rozmiar; ++j)
                Macierz[i][j] = NULL;
    }

    ~Graph()
    {
        for(int i=0; i<rozmiar; ++i)
            delete [] Macierz[i];
        delete [] Macierz;
    }

/*metody dostepu*/
    void insertVert(Typ o); //metoda dodaje nowy wierzcholek IN: o-wartosc wierzcholka
    void removeVert(Typ v); //metoda usuwa wierzcholek IN: v-wartosc wierzcholka
    void insertEdge(Typ v,Typ w,Typ o); //metoda dodaje nowy wierzcholek IN: (v,w)-krawedzie wierzcholka o-wartosc wierzcholka
    void removeEdge(Typ e); //metoda usuwa krawedz  IN: e-wartosc wierzcholka
    int endVertices(Typ); //metoda zwraca tablice dwoch koncowych wierzcholkow krawedzi e
    Vertex<Typ>* opposite(Vertex<Typ>* v, Edge<Typ>* e); //metoda wyswietla przeciwlegly wierzcholek do v wzgledem e
    bool areAdjacent(Typ v, Typ w); //jesli wierzcholki v oraz w sa sasiednie zwraca true w przeciwnym wypadku false
    void replaceVert(Typ v, Typ x); //metoda zamienia wartosci wierzcholka v oraz x
    void replaceEdge(Typ e, Typ x); //metoda zamienia wartosci krawedzi e oraz x
    Edge<Typ>* incidentEdges(Vertex<Typ>*,int); //metoda zwraca krawedzie przylegajace do v
    Vertex<Typ>* vertices(int); //metoda zwraca wskaznik na wierzcholek
    Edge<Typ>* edges(int); //metoda zwraca wskaznik na krawedz
    int sizeVert(); //metoda zwraca ilosc wierzcholkow w grafie
    int sizeEdge(); //metoda zwraca ilsc krawedzi w grafie
    int sizeIncident(Vertex<Typ>* v);//metoda zwraca ilosc wierzcholkow przylegajacych do v
    void clear(); //metoda usuwa wszystkie skladniki grafu

    friend class Edge<Typ>;
    friend class Vertex<Typ>;
};


//////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::insertVert(Typ wart)
{
    Vertex<Typ> *nowy = new Vertex<Typ>;
    nowy->wartosc = wart;
    V.addFront(nowy);
    nowy->pozycja = V.front();
}

////////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::removeVert(Typ wart)
{
    //Petla przeszukuje liste wierzcholkow w celu znalezienia usuwanego wierzcholka.
    int i;
    for(i=0; i<V.size(); i++)
        if(V[i]->wartosc == wart) break;

    if(i+1 > V.size()) throw DontExistException();

    Vertex<Typ> *wsk = V[i];
    int index_ostatniego = wsk->licznik-1;

    //Jezeli to jest wierzcholek o najwiekszym kluczu, ustawiam w macierzy sasiedztwa NULL w kolumnie i wierszu wierzcholka
    if(wsk->klucz == wsk->licznik-1)
    {
        for(int j=0; j<=index_ostatniego; j++)
        {
            Macierz[wsk->klucz][j] = NULL;
            Macierz[j][wsk->klucz] = NULL;
        }
    }
    //Jezeli to jest wierzcholek wystepujacy w wewnetrznej czasci macierzy.
    else
    {
        //Petla przeszukuje liste wierzcholkow w celu znalezienia przpisanego wierzcholka. Zamiana wartosci jego klucza na klucz usunietego wierzcholka.
        int k;
        for(k=0; k<V.size(); k++)
            if(V[k]->klucz == index_ostatniego) break;
        V[k]->klucz = wsk->klucz;

        for(int j=0; j<=index_ostatniego; j++)
        {
            Macierz[wsk->klucz][j] = NULL;
            Macierz[j][wsk->klucz] = NULL;
        }

        //Przepisuje ostatniego wierzcholka w miejsce usunietego
        for(int j=0; j<=index_ostatniego; j++)
        {
            Macierz[wsk->klucz][j] = Macierz[index_ostatniego][j];
            Macierz[j][wsk->klucz] = Macierz[j][index_ostatniego];
            Macierz[index_ostatniego][j] = NULL;
            Macierz[j][index_ostatniego] = NULL;
        }
        wsk->zmniejszLicznik();
    }
    //Usuwanie wskazanego przez uzytkownika wierzcholka
    V.erase(i);

}

////////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::insertEdge(Typ wierz_od, Typ wierz_do, Typ wart)
{
    Vertex<Typ>* wierz_Poczatkowy = findVert(wierz_od);
    Vertex<Typ>* wierz_Koncowy = findVert(wierz_do);

    if (wierz_Poczatkowy == NULL || wierz_Koncowy==NULL) throw DontExistException();
    else
    {
        Edge<Typ> *nowy = new Edge<Typ>;
        //dodwane konca i poczatku krawedzi
        nowy->poczatek = wierz_Poczatkowy;
        nowy->koniec = wierz_Koncowy;
        //dodanie wartosc nowej
        nowy->wartosc = wart;
        //dodawanie elementu do listy krawedzi
        E.addFront(nowy);
        nowy->pozycja = E.front();
        //dodawanie elementu do macierzy sasiedztwa
        int klucz_Poczatkowy = wierz_Poczatkowy->klucz;
        int klucz_Koncowy = wierz_Koncowy->klucz;
        Macierz[klucz_Poczatkowy][klucz_Koncowy] = nowy;
        Macierz[klucz_Koncowy][klucz_Poczatkowy] = nowy;
    }
}

////////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::removeEdge(Typ wart)
{

    //przeszukuje liste krawedzi w celu znalezienia krawedzi o podanej wartosci
    int i;
    for(i=0; i<E.size(); i++)
        if(E[i]->wartosc == wart) break;

    if(i+1 > E.size()) throw DontExistException();

    Edge<Typ>* wsk = E[i];
    //znajduje klucz poczatku i konca krawedzi
    int klucz_Poczatku = wsk->poczatek->klucz;
    int klucz_Konca = wsk->koniec->klucz;
    //ustawiam w macierzy sasiedztwa NULL w miejscu wystepowania polaczenia
    Macierz[klucz_Poczatku][klucz_Konca] = NULL;
    Macierz[klucz_Konca][klucz_Poczatku] = NULL;

    E.erase(i);

}

////////////////////////////////////////////////////////////////////////


template <typename Typ>
int Graph<Typ>::endVertices(Typ e)
{
    Edge<Typ>* edge = findEdge(e);
    if(edge == NULL) throw DontExistException();
    else
    {
        int tab[2];
        tab[0] = edge->poczatek->wartosc;
        tab[1] = edge->koniec->wartosc;
        return tab;
    }
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Vertex<Typ>* Graph<Typ>::opposite(Vertex<Typ>* vertex, Edge<Typ>* edge)
{
    if(edge->poczatek->wartosc == vertex->wartosc)
        return edge->koniec;
    else if(edge->koniec->wartosc == vertex->wartosc)
        return edge->poczatek;
}

////////////////////////////////////////////////////////////////////////

template <typename Typ>
bool Graph<Typ>::areAdjacent(Typ v, Typ w)
{
    Vertex<Typ>* vertex1 = findVert(v);
    Vertex<Typ>* vertex2 = findVert(w);

    //Warunek sprawdza czy znaleziono wierzcholki
    if (vertex1 == NULL || vertex2 == NULL) throw DontExistException();

    int klucz1 = vertex1->klucz;
    int klucz2 = vertex2->klucz;

    if(Macierz[klucz1][klucz2] != NULL || Macierz[klucz2][klucz1] != NULL)
        return true;
    else
        return false;
}

////////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::replaceVert(Typ v, Typ x)
{
    Vertex<Typ>* vertex = findVert(v);

    if(vertex == NULL) throw DontExistException();
    else vertex->wartosc = x;
}

////////////////////////////////////////////////////////////////////////

template <typename Typ>
void Graph<Typ>::replaceEdge(Typ e, Typ x)
{
    Edge<Typ>* edge = findEdge(e);

    if(edge == NULL) throw DontExistException();
    else edge->wartosc = x;
}

////////////////////////////////////////////////////////////////////////

template <typename Typ>
Edge<Typ>* Graph<Typ>::incidentEdges(Vertex<Typ>* v, int iter)
{
    int pomoc = 0; //zmienna pomocnicza zliczajaca ilosc znalezionych krawedzi
    int i;
    for(i=0; i<v->licznik; i++)
    {
        if(Macierz[i][v->klucz] != NULL)
        {
            if(iter == pomoc)
                return Macierz[i][v->klucz];
            pomoc++;
        }
    }
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Vertex<Typ>* Graph<Typ>::vertices(int iter)
{
    return V[iter];
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Edge<Typ>* Graph<Typ>::edges(int iter)
{
    return E[iter];
}

//////////////////////////////////////////////////////////////////////

template <typename Typ>
Vertex<Typ>* Graph<Typ>::findVert(Typ wart)
{
    int i;
    for(i=0; i<V.size(); i++)
        if(V[i]->wartosc == wart) break;

    if(i+1 > V.size()) return NULL;
    else return V[i];
}

template <typename Typ>
Edge<Typ>* Graph<Typ>::findEdge(Typ wart)
{
    int i;
    for(i=0; i<E.size(); i++)
        if(E[i]->wartosc == wart) break;


    if(i+1 > E.size()) return NULL;
    else return E[i];
}

template <typename Typ>
int Graph<Typ>::sizeVert()
{
    return V.size();
}

template <typename Typ>
int Graph<Typ>::sizeEdge()
{
    return E.size();
}

template <typename Typ>
int Graph<Typ>::sizeIncident(Vertex<Typ>* v)
{
    int pomoc = 0; //zmienna pomocnicza zliczajaca ilosc znalezionych krawedzi
    for(int i=0; i<v->licznik; i++)
        if(Macierz[i][v->klucz] != NULL) pomoc++;
    return pomoc;

}

template <typename Typ>
void Graph<Typ>::clear()
{
    vertices(0)->wyzerujLicznik();
    E.clear();
    V.clear();
    for(int i=0; i<rozmiar; ++i)
        for(int j=0; j<rozmiar; ++j)
            Macierz[i][j] = NULL;
}

