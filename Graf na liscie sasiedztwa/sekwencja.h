#include <iostream>
#include <exception>


using namespace std;

class sekwencjaEmptyException;
class SizeOfsekwencjaException;
class DontExistsekwencjaException;

template <typename T> class Sekwencja;

template <typename T>
class wezel
{
private:
	T wartosc;
	wezel * nastepny;
	wezel * poprzedni;
	friend class Sekwencja<T>;

public:

    /******************metody dostepu do zmiennych*******************/


    T getWartosc() const {return wartosc;} //metoda dostapu do wartosci
    wezel* getNastepny() const {return nastepny;} //metoda dostepu do nastepnego
    wezel* getPoprzedni() const {return poprzedni;} //metoda dostepu do poprzedniego
    void setWartosc(T nowyW) {wartosc = nowyW;} //metoda modyfikujaca wartosc
    void setNastepny(wezel<T> *nowyN) {nastepny = nowyN;} //metoda modyfikuja nastepny
    void setPoprzedni(wezel<T> *nowyP) {poprzedni = nowyP;} //metoda modyfikuja poprzedni

    wezel()
	{
		nastepny = NULL;
		poprzedni = NULL;
	}
};

template <typename T>
class Sekwencja
{
private:
    wezel <T> *glowa;
    wezel <T> *ogon;
    int rozmiar;

public:

    /******************metody dostepu do zmiennych*******************/

    wezel<T>* getGlowa() const {return glowa;} //metoda dostepu do glowy
    wezel<T>* getOgon() const {return ogon;} //metoda dostepu do ogona
    int getRozmiar() const {return rozmiar;} //metoda dostepu do rozmiaru
    void setGlowa(wezel<T> *nowyG) {glowa = nowyG;} //metoda modyfikujaca glowa
    void setOgon(wezel<T> *nowyO) {ogon = nowyO;} //metoda modyfikujaca ogon
    void setRozmiar(int r) {rozmiar = r;} //metoda modyfikujaca liczby elementow

    int size() const; //Zwraca ilo�� obiekt�w przechowywanych w sekwencji
    bool isEmpty() const; //Zwraca true je�li sekwencja jest pustq
    T & front() const; // Zwraca pierwszy obiekt w sekwencji. (Wyrzuca SekwencjaEmptyException je�li sekwencja jest pusta)
    T & back() const; // Zwraca ostatni obiekt w Sekwencja. (Wyrzuca SekwencjaEmptyException je�li sekwencja jest pusta)
    void addFront(const T&); //Dodaje obiekt do poczatku sekwencji. IN: element, ktory chcemy wstawic
    Sekwencja <T> & removeFront(); // Usuwa pierwszy obiekt z sekwencji. (Wyrzuca SekwencjaEmptyException je�li sekwencja jest pusta)
    void addBack(const T&); //Dodaje obiekt na ko�cu sekwencji. IN: element, ktory chcemy wstawic
    Sekwencja <T> & removeBack(); //Usuwa ostatni obiekt z sekwencji. (Wyrzuca SekwencjaEmptyException je�li sekwencja jest pusta)
    void insert(int,T); //Dodaje element w miejsce o podanym indeksie i wartosci.
    void erase(int); //Usuwa element o podanym indeksie. IN: indeks elementu ktory chemu usunac
    void clear(); //Usuwa wszystkie elementy
    T operator [] (int); //zwraca element listy o podanym indeksie (Wyrzuca SizeOfListException je�li sekwencja jest pusta)
    T atIndex(int i) const; //zwraca element z pozycji i IN: indeks elementu OUT: wartosc elementu
    int indexOf(T p); //zwraca indeks elementu IN: wartosc elementu OUT: indeks elementu


    Sekwencja() //konstruktor
    {
        glowa = NULL;
        ogon = NULL;
        rozmiar = 0;
    }
    ~Sekwencja() //destruktor
    {
        clear();
    }
};

template <typename T>
int Sekwencja<T>::size() const
{
    return rozmiar;
}

template <typename T>
bool Sekwencja<T>::isEmpty() const
{
    if(rozmiar==0) return true;
    else return false;
}

template <typename T>
T & Sekwencja<T>::front() const
{
    if(rozmiar==0) throw sekwencjaEmptyException();
    else return glowa->wartosc;
}

template <typename T>
T & Sekwencja<T>::back() const
{
    if(rozmiar==0) throw sekwencjaEmptyException();
    else return ogon->wartosc;
}

template <typename T>
void Sekwencja<T>::addFront(const T & elem)
{
	wezel <T> *nowy = new wezel <T>;
    nowy->wartosc = elem; //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa = nowy;
        ogon = nowy;
    }
    else
    {
        nowy->nastepny = glowa;
        glowa->poprzedni = nowy;
        glowa = nowy;
    }
    rozmiar++;
}

template <typename T>
Sekwencja<T> & Sekwencja<T>::removeFront()
{
    wezel <T> *wsk = glowa;
    if(rozmiar==0) throw sekwencjaEmptyException();
    else
    {
        glowa = glowa->nastepny;
        delete wsk;
        --rozmiar;
        return *this;
    }
}

template <typename T>
void Sekwencja<T>::addBack(const T & elem)
{
	wezel <T> *nowy = new wezel <T>;
    nowy->wartosc = elem; //ustawia wartosc kontenera

    if(rozmiar==0)
    {
        glowa=nowy;
        ogon=nowy;
    }
    else
    {
        ogon->nastepny = nowy;
        nowy->poprzedni = ogon;
        ogon = nowy;
    }
    rozmiar++;
}

template <typename T>
Sekwencja<T> & Sekwencja<T>::removeBack()
{
    wezel <T> *wsk = ogon;
    if(rozmiar==0) throw sekwencjaEmptyException();
    else
    {
        ogon = ogon->poprzedni;
        delete wsk;
        rozmiar--;
    }
    return *this;
}

template <typename T>
void Sekwencja<T>::erase(int index)
{
    if(index >= rozmiar || index < 0) throw SizeOfsekwencjaException();
    else if (index == 0) removeFront();
    else if (index == rozmiar-1) removeBack();
    else
    {
        wezel<T> *wsk1 = glowa;
        wezel<T> *wsk2 = glowa->nastepny;
        wezel<T> *wsk3 = glowa->nastepny->nastepny;
        for(int i=1; i<index; i++)
        {
            wsk1 = wsk1->nastepny;
            wsk2 = wsk2->nastepny;
            wsk3 = wsk3->nastepny;
        }

        wsk1->nastepny = wsk3;
        wsk3->poprzedni = wsk1;
        delete wsk2;
        rozmiar--;
    }
}

template <typename T>
void Sekwencja<T>::insert(int index, T wart)
{
    if(index >= rozmiar || index < 0) throw SizeOfsekwencjaException();
    else if (index == 0) addFront(wart);
    else if (index == rozmiar-1) addBack(wart);
    else
    {
        wezel<T> *wsk1 = glowa;
        wezel<T> *wsk2 = glowa->nastepny;
        wezel<T> *nowy = new wezel<T>;
        for(int i=1; i<index; i++)
        {
            wsk1 = wsk1->nastepny;
            wsk2 = wsk2->nastepny;
        }

        nowy->wartosc = wart;
        nowy->poprzedni = wsk1;
        nowy->nastepny = wsk2;
        wsk1->nastepny = nowy;
        wsk2->poprzedni = nowy;

        rozmiar++;
    }
}

template <typename T>
void Sekwencja<T>::clear()
{
    while(!isEmpty()) removeFront();
}

template <typename T>
T Sekwencja<T>::operator[](int index)
{
    wezel<T>* wsk = glowa;
    if(index < 0 || index >= rozmiar) throw SizeOfsekwencjaException();
    else
    {
        for(int i=0; i<index; i++)
            wsk = wsk->nastepny;

        return wsk->wartosc;
    }
}

template <typename T>
T Sekwencja<T>::atIndex(int i) const
{
    wezel<T>* wsk = glowa;
    if(isEmpty()) throw SizeOfsekwencjaException();
    else
    {
        for(int j=1; j<i; j++)
            wsk = wsk->getNastepny();
        return wsk->getWartosc();
    }
}

template <typename T>
int Sekwencja<T>::indexOf(T p)
{
    wezel<T>* wsk = glowa;
    int index = 1;

    while(wsk->getWartosc()!=p && (index < size()))
    {
        ++index;
        wsk = wsk->next;
    }
    if(index==size()) throw SizeOfsekwencjaException();
    else return index;
}


class sekwencjaEmptyException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Sekwencja jest pusta.";
	}
};

class SizeOfsekwencjaException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Przekroczyles rozmiar.";
	}
};

class DontExistsekwencjaException : public std::exception
{
public:
	virtual const char * what() const noexcept
	{
		return "Element nie istnieje.";
	}
};
